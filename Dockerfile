FROM composer:latest AS composer

FROM php:7.4-fpm

COPY --from=composer /usr/bin/composer /usr/local/bin/composer

# Install dependencies
RUN apt-get update && apt-get install -y zip unzip # libicu-dev

# Clear cache
RUN apt-get clean && rm -rf /var/lib/apt/lists/*

# Install extensions
RUN pecl install xdebug
RUN docker-php-ext-enable xdebug
# RUN docker-php-ext-install zip exif pcntl

# Add user for laravel application
RUN groupadd -g 1000 www
RUN useradd -u 1000 -ms /bin/bash -g www www

# Set working directory
WORKDIR /var/www/

# Copy composer.lock and composer.json
COPY composer.lock composer.json /var/www/

# Copy existing application directory permissions
COPY --chown=www:www . /var/www/

# Change current user to www
USER www

# Expose port 9000 and start php-fpm server
EXPOSE 9000
# CMD ["php-fpm"]
