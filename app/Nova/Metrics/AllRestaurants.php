<?php

namespace App\Nova\Metrics;

use App\Model\Restaurant;
use Illuminate\Http\Request;
use Laravel\Nova\Metrics\Value;

class AllRestaurants extends Value
{

	public $name = 'Registrované restaurace';

	/**
	 * Calculate the value of the metric.
	 *
	 * @param  \Illuminate\Http\Request $request
	 *
	 * @return mixed
	 */
	public function calculate(Request $request)
	{
		return $this->count($request, Restaurant::class);
	}

	/**
	 * Get the ranges available for the metric.
	 *
	 * @return array
	 */
	public function ranges()
	{
		return [
			365 => '365 Days',
			60  => '60 Days',
			30  => '30 Days',
			7   => 'Last week',
			//'YTD' => 'Year To Date',
			//'QTD' => 'Quarter To Date',
			//'MTD' => 'Month To Date',
		];
	}

	/**
	 * Determine for how many minutes the metric should be cached.
	 *
	 * @return  \DateTimeInterface|\DateInterval|float|int
	 */
	public function cacheFor()
	{
		return now()->addHours(6);
	}

	/**
	 * Get the URI key for the metric.
	 *
	 * @return string
	 */
	public function uriKey()
	{
		return 'all-restaurants';
	}
}
