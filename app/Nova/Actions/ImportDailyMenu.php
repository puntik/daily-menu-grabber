<?php

namespace App\Nova\Actions;

use App\Jobs\ImportDailyMenuJob;
use App\Model\Restaurant;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Collection;
use Laravel\Nova\Actions\Action;
use Laravel\Nova\Fields\ActionFields;

class ImportDailyMenu extends Action
{

    use InteractsWithQueue, Queueable, SerializesModels;

    public function handle(ActionFields $fields, Collection $restaurants): void
    {
        /** @var Restaurant $restaurant */
        foreach ($restaurants as $restaurant) {
            if ($restaurant->status === false) {
                return;
            }

            dispatch(new ImportDailyMenuJob($restaurant));
        }
    }

    /**
     * Get the fields available on the action.
     *
     * @return array
     */
    public function fields()
    {
        return [];
    }
}
