<?php

namespace App\Nova;

use App\Nova\Actions\ImportDailyMenu;
use App\Services\Grabbers\FerdinandaGrabber;
use App\Services\Grabbers\NaRozhraniGrabber;
use Illuminate\Http\Request;
use Laravel\Nova\Fields\Boolean;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Select;
use Laravel\Nova\Fields\Text;

class Restaurant extends Resource
{

    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = 'App\Model\Restaurant';

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'name';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'id',
        'name',
    ];

    public function subtitle()
    {
        return 'Url: ' . $this->menu_url;
    }

    /**
     * Get the fields displayed by the resource.
     *
     * @param Request $request
     *
     * @return array
     */
    public function fields(Request $request)
    {
        $grabbers = $this->grabberOptions();

        return [
            ID::make()->sortable(),
            Text::make('Name')->sortable(),
            Select::make('Grabber')->options($grabbers)->hideFromIndex(),
            Text::make('Menu url', 'menu_url')->rules('required', 'url')->hideFromIndex(),
            Boolean::make('Active', 'status'),
        ];
    }

    private function grabberOptions(): array
    {
        return [
            FerdinandaGrabber::class => FerdinandaGrabber::class,
            NaRozhraniGrabber::class => NaRozhraniGrabber::class,
        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param Request $request
     *
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param Request $request
     *
     * @return array
     */
    public function filters(Request $request)
    {
        return [
        ];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param Request $request
     *
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param Request $request
     *
     * @return array
     */
    public function actions(Request $request)
    {
        return [
            new ImportDailyMenu(),
        ];
    }
}
