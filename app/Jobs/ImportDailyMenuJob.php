<?php

namespace App\Jobs;

use App\Model\Restaurant;
use App\Services\Grabbers\Grabber;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;

class ImportDailyMenuJob implements ShouldQueue
{

    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private Restaurant $restaurant;

    public function __construct(Restaurant $restaurant)
    {
        $this->restaurant = $restaurant;
    }

    public function handle()
    {
        /** @var Grabber $grabber */
        $grabber = app()->get($this->restaurant->grabber);

        $result = $grabber->grab();
        Cache::put($this->restaurant->cache_key, $result);

        Log::info(sprintf(
                'Downloaded and cached daily menu for restaurant #%s: "%s" with key "%s".',
                $this->restaurant->id,
                $this->restaurant->name,
                $this->restaurant->cache_key
            )
        );
    }
}
