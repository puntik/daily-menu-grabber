<?php declare(strict_types = 1);

namespace App\Services;

use GuzzleHttp\Client;

class SimpleBodyDownloader
{

	/** @var Client */
	private $client;

	public function __construct(Client $client)
	{
		$this->client = $client;
	}

	public function getBody(string $url): string
	{
		$response = $this->client->get($url);

		return $response->getBody()->getContents();
	}
}
