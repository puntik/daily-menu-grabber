<?php declare(strict_types = 1);

namespace App\Services\Grabbers;

use GuzzleHttp\Client;

class NaHradcanskeGrabber implements Grabber
{

	private const NAME = 'Na Hradčanské';

	/** @var Client */
	private $httpClient;

	public function __construct(Client $httpClient)
	{
		$this->httpClient = $httpClient;
	}

	public function grab(): array
	{
		$url      = 'https://www.nahradcanske.cz/denni-menu-xml/';
		$response = $this->httpClient->get($url);
		$content  = $response->getBody()->getContents();
		$crawler  = new \Symfony\Component\DomCrawler\Crawler($content);
		$menuPost = $crawler->filter('.menu_post');

		$menu = [];

		foreach ($menuPost as $row) {
			$menu[] = [
				'meat'  => $row->childNodes->item(1)->textContent,
				'price' => $row->childNodes->item(5)->textContent,

			];
		}

		return [
			'pub'  => self::NAME,
			'menu' => $menu,
		];
	}
}
