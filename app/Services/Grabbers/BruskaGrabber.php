<?php declare(strict_types = 1);

namespace App\Services\Grabbers;

use App\Services\Zomato\Client;

class BruskaGrabber implements Grabber
{

	private const NAME = 'Restaurant Bruska';

	/** @var int */
	private $zomatoRestaurantId = 16507153;

	/** @var Client */
	private $zomatoClient;

	public function __construct(Client $zomatoClient)
	{
		$this->zomatoClient = $zomatoClient;
	}

	public function grab(): array
	{
		$menu = $this->parseSite($this->zomatoRestaurantId);

		return [
			'pub'  => self::NAME,
			'menu' => $menu,
		];
	}

	private function parseSite(int $restaurantId): array
	{
		$json   = $this->zomatoClient->dailyMenu($restaurantId);
		$dishes = $json['daily_menus'][0]['daily_menu']['dishes'];

		$menu = array_filter($dishes, function ($item) {
			$dish = $item['dish'];

			if (preg_match('/\d{2,3}\sKč/u', $dish['price'])) {
				return $dish;
			}

			return null;
		});

		return array_map(function ($item) {
			return [
				'meat'  => trim($item['dish']['name']),
				'price' => $item['dish']['price'],
			];
		}, $menu);
	}
}
