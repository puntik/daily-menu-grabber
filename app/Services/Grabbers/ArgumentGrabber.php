<?php declare(strict_types = 1);

namespace App\Services\Grabbers;

use App\Services\Grabbers\Traits\NeedsCzechWeekdayNames;
use GuzzleHttp\Client;
use Illuminate\Support\Carbon;
use Symfony\Component\DomCrawler\Crawler;

class ArgumentGrabber implements Grabber
{

	use NeedsCzechWeekdayNames;

	const NAME = 'Argument';

	/** @var Client */
	private $client;

	public function __construct(Client $client)
	{
		$this->client = $client;
	}

	public function grab(): array
	{
		$menu = $this->parsePage();

		return [
			'pub'  => self::NAME,
			'menu' => $menu,
		];
	}

	private function parsePage(): array
	{
		$url  = 'http://www.argument-restaurant.cz/tydenni-nabidka-1/';
		$body = $this->client->get($url)->getBody()->getContents();
		$body = str_replace('&nbsp;', ' ', $body);

		$crawler = new Crawler($body);

		$rows = $crawler->filter('.text .datatable tbody tr');

		$weekly = $this->grabWeeklyMenu($rows);
		$daily  = $this->grabDailyMenu($rows);

		return array_merge($daily, $weekly);
	}

	private function grabDailyMenu(Crawler $crawler)
	{
		$dailyIterator = 1;

		$data = $crawler->each(function (Crawler $nodes) {
			$columns = $nodes->filter('td');

			$meat  = trim($columns->getNode(0)->textContent);
			$price = $columns->getNode(1) !== null ? trim($columns->getNode(1)->textContent) : '';

			return [
				'meat'  => $meat,
				'price' => $price,
			];
		});

		$daily = array_filter($data, function ($item) use (&$dailyIterator) {

			$day     = (int) Carbon::today()->format('N');
			$dayname = $this->czechWeekdayName($day);

			if (preg_match('#' . $dayname . '#u', $item['meat'])) {
				$dailyIterator++;

				return [];
			}

			if ($dailyIterator == 2) {
				$dailyIterator++;

				return [];
			}

			if ($dailyIterator === 3) {
				$dailyIterator = 0;

				return $item;
			}
		});

		$daily = array_shift($daily);

		$meats  = explode("\n", $daily['meat']);
		$prices = explode("\n", $daily['price']);

		$resp = [];

		foreach ($meats as $meat) {
			if (trim($meat) !== '') {
				$resp[] = ['meat' => trim($meat)];
			}
		}

		$priceIterator = 0;
		foreach ($prices as $price) {
			if (trim($price) !== '') {
				$resp[$priceIterator++]['price'] = trim($price);
			}
		}

		return $resp;
	}

	private function grabWeeklyMenu(Crawler $rows)
	{
		$data = $rows->each(function (Crawler $nodes) {
			$columns = $nodes->filter('td');

			$meat  = trim($columns->getNode(0)->textContent);
			$price = $columns->getNode(1) !== null ? trim($columns->getNode(1)->textContent) : '';

			return [
				'meat'  => $meat,
				'price' => $price,
			];
		});

		$grab = false;

		return array_filter($data, function ($item) use (&$grab) {
			$weeklyHeader = 'Týdenní nabídka obědů';

			if ($item['meat'] === $weeklyHeader) {
				$grab = true;
			}

			if ($grab && $item['meat'] !== $weeklyHeader && $item['price'] !== '') {
				return $item;
			}

			return null;
		});
	}
}
