<?php
declare(strict_types=1);

namespace App\Services\Grabbers;

use Illuminate\Support\Arr;

class GrabbersAggregator {

    private $grabbers;

    public function __construct($grabbers) {
        $this->grabbers = $grabbers;
    }

    public function getGrabbers(): \Traversable {
        return $this->grabbers;
    }

    /**
     * @return Grabber[]|array
     */
    public function grabbers(): array {
        return array_values($this->grabbers);
    }

    /**
     * @param string $name
     *
     * @return Grabber
     * @throws \Throwable
     */
    public function grabber(string $name): Grabber {
        throw_unless(
            Arr::has($this->grabbers, $name),
            new \InvalidArgumentException(
                sprintf(
                    'Requested grabber "%s" does not exist or it is not registered in the service provider.',
                    $name
                )
            )
        );

        return $this->grabbers[$name];
    }

    /**
     * @return string[]|array
     */
    public function list(): array {
        return array_keys($this->grabbers);
    }

}
