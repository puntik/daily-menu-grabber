<?php declare(strict_types = 1);

namespace App\Services\Grabbers;

use App\Model\Dish;
use App\Model\Menu;
use GuzzleHttp\Client;
use Symfony\Component\DomCrawler\Crawler;

class FerdinandaGrabber implements Grabber
{

    private const NAME = 'Ferdinanda Nové Město';

    private Client $client;

    private array $unwanted;

    public function __construct(Client $client)
    {
        $this->client = $client;

        $this->unwanted = [
            'Příloha navíc',
            'Krabička a taška na odnos jídla',
        ];
    }

    public function grab(): array
    {
        $url = 'http://www.ferdinanda.cz/cs/nove-mesto/menu/denni-menu';

        $body = $this->client->get($url)->getBody()->getContents();

        $crawler = new Crawler($body);
        $rawMenu = $crawler->filter('.menu_D tr')->each(function (Crawler $node): ?Dish {
            $row = $node->getNode(0);

            $meal  = $row->childNodes->item(1) ? $row->childNodes->item(1)->textContent : null;
            $price = $row->childNodes->item(2) ? $row->childNodes->item(2)->textContent : null;

            if (empty($meal)) {
                return null;
            }

            // remove unwanted texts
            if (in_array($meal, $this->unwanted)) {
                return null;
            }

            return new Dish($meal, empty($price) ? 0 : $price);
        }); // end of each

        return (new Menu(self::NAME, array_filter($rawMenu)))->toArray();
    }
}
