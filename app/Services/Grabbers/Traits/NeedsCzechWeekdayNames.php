<?php declare(strict_types = 1);

namespace App\Services\Grabbers\Traits;

trait NeedsCzechWeekdayNames
{

	public function czechWeekdayName(int $order): string
	{
		$names = [
			'Pondělí',
			'Úterý',
			'Středa',
			'Čtvrtek',
			'Pátek',
			'Sobota',
			'Neděle',
		];

		return $names[$order - 1];
	}
}
