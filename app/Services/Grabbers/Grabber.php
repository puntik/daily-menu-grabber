<?php declare(strict_types = 1);

namespace App\Services\Grabbers;

interface Grabber
{

	/**
	 * <pre>
	 * [
	 *    'pub' => 'pub_name',
	 *    'menu' => [
	 *       [
	 *            'meat' => 'meat_1',
	 *            'price' => '120 Kc',
	 *            'allergens' => [],
	 *       ],
	 *       [
	 *            'meat' => 'meat_2',
	 *            'price' => '106 Kc',
	 *            'allergens' => [],
	 *       ],
	 *    ]
	 * ]
	 * </pre>
	 *
	 *
	 * @return array
	 */
	public function grab(): array;
}
