<?php declare(strict_types = 1);

namespace App\Services\Grabbers;

use Symfony\Component\DomCrawler\Crawler;

class SokolovnaDejviceGrabber implements Grabber
{

	private const NAME = 'Sokolovna Dejvice';

	/** @var \GuzzleHttp\Client */
	private $httpClient;

	public function __construct(\GuzzleHttp\Client $httpClient)
	{
		$this->httpClient = $httpClient;
	}

	public function grab(): array
	{
		$menu = $this->processSourcePage();

		return [
			'pub'  => self::NAME,
			'menu' => $menu,
		];
	}

	private function processSourcePage()
	{
		// stahnout jidelak http://www.sokolovna.cz/denni-menu.aspx
		$url  = 'http://www.sokolovna.cz/denni-menu.aspx';
		$body = $this->httpClient->get($url)->getBody()->getContents();

		// zpracovat
		$crawler  = new Crawler($body);
		$menuRows = $crawler->filter('.deniMenuTable tbody tr');

		$menu = $menuRows->each(function (Crawler $row) {
			$columns = $row->filter('td');

			if ($columns->count() > 1) {
				$meat  = $columns->getNode($columns->count() - 2)->textContent;
				$price = $columns->getNode($columns->count() - 1)->textContent;

				$price = str_replace(',-', ' Kč', $price);

				return [
					'meat'  => $meat,
					'price' => $price,
				];
			}

			return null;
		});

		return array_filter($menu, function ($item) {
			if ($item !== null) {
				return $item;
			}

			return null;
		});
	}

}
