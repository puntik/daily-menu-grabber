<?php declare(strict_types = 1);

namespace App\Services\Grabbers;

use App\Services\Tesseract\Scanner;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Str;

class UVeverkyGrabber implements Grabber
{

	private const NAME = 'U Veverky (beta)';

	/** @var Client */
	private $httpClient;

	/** @var Scanner */
	private $scanner;

	public function __construct(
		Client $httpClient,
		Scanner $scanner
	) {
		$this->httpClient = $httpClient;
		$this->scanner    = $scanner;
	}

	public function grab(): array
	{
		$cacheKey = sprintf('pubs:%s', Str::slug(self::NAME));

		$menu = Cache::remember($cacheKey, 180, function () {
			return $this->parseMenu();
		});

		// returns
		return [
			'pub'  => self::NAME,
			'menu' => $menu,
		];
	}

	private function parseMenu()
	{
		// download image (preprocess)
		$image = $this->downloadImage();

		// scan image with tesseract (preprocess)
		$input = $this->scan($image);

		// parse txt output from scanning
		$body = file_get_contents($input);

		// process body to menu
		$data = $this->processBody($body);

		return array_filter($data, function ($item) {
			if (strlen($item['meat']) < 16) {
				return null;
			}

			return $item;
		});
	}

	private function processBody(string $body)
	{
		// patern1 - it parses lines with price
		// regexr: ^(?:\d*)(ks|g|8)(?:\s?—?\s?)?([^\d]+?)(?:\s(?:v\s)?(?:—\s)?)?(\d*)(?:,—)
		$pattern1 = '/^(?:\d*)(ks|g|\d+)(?:\s?—?\s?)?([^\d]+?)(?:\s(?:v\s)?(?:—\s)?)?(\d*)(?:,—)/u';
		preg_match_all($pattern1, $body, $matches, PREG_SET_ORDER);

		// pattern bez ceny
		// regexr: ^(?:\d*)(ks|g|8|\d)(?:\s?—?\s?)?(.*)/u
		if (count($matches) === 0) {
			$pattern2 = '/(?:\d*)(?:ks|g|\d+)\s(?:\—?\s)\b(.*)/mu';
			preg_match_all($pattern2, $body, $matches, PREG_SET_ORDER);

			return array_map(function ($item) {
				return [
					'meat'  => $item[1],
					'price' => 0,
				];
			}, $matches);
		}

		return array_map(function ($item) {
			return [
				'meat'  => $item[2],
				'price' => ($item[3] ?? '-') . ' Kč',
			];
		}, $matches);
	}

	private function downloadImage(): string
	{
		$date = '181011';
		$link = sprintf('http://www.uveverky.com/img/gallery/%s%%20poledn%%C3%%AD%%20nabidka.jpg', $date);

		$image = storage_path(sprintf('u-veverky-%s.jpg', $date));

		$this->httpClient->get($link, ['save_to' => $image]);

		return $image;
	}

	private function scan(string $image)
	{
		$date = '181011';
		$path = storage_path('app/u-veverky-' . $date);

		return $this->scanner->scan($image, $path);
	}
}
