<?php declare(strict_types = 1);

namespace App\Services\Grabbers;

use App\Model\Dish;
use App\Model\Menu;
use GuzzleHttp\Client;
use Symfony\Component\DomCrawler\Crawler;

class NaRozhraniGrabber implements Grabber
{

    private const NAME = 'Na rozhraní';

    /** @var Client */
    private $client;

    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    public function grab(): array
    {
        $url        = 'http://www.narozhrani.cz/';
        $body       = $this->client->get($url)->getBody()->getContents();
        $crawler    = new Crawler($body);
        $menuFilter = $crawler->filter('#obsah tr');

        $dishes = $menuFilter->each(function (Crawler $node) {
            $columns = $node->filter('td');

            return new Dish(
                $columns->getNode(0)->textContent,
                $columns->getNode(1)->textContent
            );
        });

        return (new Menu(self::NAME, $dishes))->toArray();
    }
}
