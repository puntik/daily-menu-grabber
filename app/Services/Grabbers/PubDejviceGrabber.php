<?php declare(strict_types = 1);

namespace App\Services\Grabbers;

use App\Services\Grabbers\Traits\NeedsCzechWeekdayNames;
use GuzzleHttp\Client as HttpClient;
use Illuminate\Support\Carbon;
use Symfony\Component\DomCrawler\Crawler;

class PubDejviceGrabber implements Grabber
{

	use NeedsCzechWeekdayNames;

	const NAME = 'PUB Dejvice';

	private $grab;

	/** @var HttpClient */
	private $httpClient;

	public function __construct(HttpClient $httpClient)
	{
		$this->httpClient = $httpClient;
		$this->grab       = false;
	}

	public function grab(): array
	{
		$url  = 'http://www.thepub.cz/praha-6/poledni-menu/';
		$body = $this->httpClient->get($url)->getBody()->getContents();

		$crawler = new Crawler($body);

		$menu = $crawler->filter('table.menu tr');

		$day      = (int) Carbon::today()->format('N');
		$today    = $this->czechWeekdayName($day);
		$tomorrow = $this->czechWeekdayName($day + 1);

		$data = $menu->each(function (Crawler $row) use ($today, $tomorrow) : ?array {

			if (preg_match('/' . $today . '/iu', $row->text())) {
				$this->grab = true;

				return null;
			}
			if (preg_match('/' . $tomorrow . '/iu', $row->text())) {
				$this->grab = false;

				return null;
			}

			if ($this->grab === false) {
				return null;
			}

			$columns = $row->filter('td');

			if ($columns->count() !== 3) {
				return null;
			}

			// <strong><span class="right">150g</span>Grilovaná kuřecí prsíčka v indickém Tandoori koření, podávané s basmati rýží (A1,A7)</strong><div class="cl">
			$meatRaw = $columns->filter('.item')->html();
			if (preg_match(
				'#<strong><span class="right">.*</span>(.*)\s*\((.*)\)</strong>#iu',
				$meatRaw,
				$matches
			)) {
				$allergens = explode(',', str_replace('A', '', $matches[2]));

				return [
					'meat'      => trim($matches[1]),
					'price'     => $columns->filter('.price')->text(),
					'allergens' => $allergens,
				];
			}

			return null;
		});

		return [
			'pub'  => self::NAME,
			'menu' => array_filter($data),
		];
	}
}
