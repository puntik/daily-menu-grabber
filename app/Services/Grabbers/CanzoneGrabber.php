<?php declare(strict_types = 1);

namespace App\Services\Grabbers;

use App\Services\Grabbers\Traits\NeedsCzechWeekdayNames;
use GuzzleHttp\Client;
use Illuminate\Support\Carbon;
use Symfony\Component\DomCrawler\Crawler;

class CanzoneGrabber implements Grabber
{

	use NeedsCzechWeekdayNames;

	private const NAME = 'Canzone';

	/** @var Client */
	private $client;

	public function __construct(Client $client)
	{
		$this->client = $client;
	}

	public function grab(): array
	{
		$menu = $this->parsePage();

		return [
			'pub'  => self::NAME,
			'menu' => $menu,
		];
	}

	private function parsePage()
	{
		$url  = 'http://canzone.cz/poledni-menu/';
		$body = $this->client->get($url)->getBody()->getContents();
		$body = str_replace(['&nbsp;', '<br />', "\n"], ' ', $body);

		return $this->getMenu($body);
	}

	private function getMenu(string $body)
	{
		$crawler = new Crawler($body);

		$menu = $crawler->filter('.polednimenu')->children();

		$parseStart = false;

		$x = $menu->each(function (Crawler $crawler) use (&$parseStart) {
			$line    = $crawler->text();
			$day     = (int) Carbon::today()->format('N');
			$dayname = $this->czechWeekdayName($day);

			if (preg_match('#^' . $dayname . '#', $line)) {
				$parseStart = true;

				return null;
			}

			if ($parseStart && $line === '') {
				$parseStart = false;

				return null;
			}

			if ($parseStart) {
				preg_match('#^(.*)\s\*?(\d*?)\,\-\s*Kč#u', $line, $matches);
				preg_match('#^(.*)\(#u', $matches[1], $y);

				return [
					'meat'  => trim($y[1] ?? $matches[1]),
					'price' => $matches[2] . ' Kč',
				];
			}

			return null;
		});

		return array_filter($x, function ($item) {
			if ($item !== null) {
				return $item;
			}
		});
	}
}
