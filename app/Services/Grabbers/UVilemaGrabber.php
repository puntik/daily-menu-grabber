<?php declare(strict_types = 1);

namespace App\Services\Grabbers;

use GuzzleHttp\Client;
use Symfony\Component\DomCrawler\Crawler;

class UVilemaGrabber implements Grabber
{

	private const NAME = 'U Viléma';

	/** @var Client */
	private $client;

	public function __construct(Client $client)
	{
		$this->client = $client;
	}

	public function grab(): array
	{
		$menu = $this->parsePage();

		return [
			'pub'  => self::NAME,
			'menu' => $menu,
		];
	}

	private function parsePage()
	{
		$url  = 'https://www.restauraceuvilema.cz/';
		$body = $this->client->get($url)->getBody()->getContents();
		$body = str_replace('&nbsp;', ' ', $body);

		$crawler = (new Crawler($body))->filter('.section-inner .content .text .text-content p');

		$data = $crawler->each(function (Crawler $crawler) {
			$line = $crawler->getNode(0)->textContent;
			$line = preg_replace("/\r|\n/", ' ', $line);

			if (preg_match('/^(\d+\,?\w+)\s+(.*)/', $line, $matches)) {
				$text = $matches[2];

				if (preg_match('#(.*)\s*(\d\d\d)\,\-\s?Kč#u', $text, $res)) {
					return $this->prepareResponse($res);
				}

				if (preg_match('#(.*)\s*(\d\d)\,\-\s?Kč#u', $text, $res)) {
					return $this->prepareResponse($res);
				}
			}

			return null;
		});

		return array_filter($data);
	}

	private function prepareResponse(array $res)
	{
		$parsed = $this->expandeAllergens($res[1]);

		if ($parsed['meat'] === null) {
			return null;
		}

		return [
			'meat'      => $parsed['meat'],
			'price'     => $res[2] . ' Kč',
			'allergens' => [],
		];
	}

	private function expandeAllergens(string $input): ?array
	{
		if (preg_match('#([^/]+?)/(.*)/#iu', $input, $matches)) {
			return [
				'meat'      => trim($matches[1]),
				'allergens' => explode(',', $matches[2]),
			];
		}

		return null;
	}
}
