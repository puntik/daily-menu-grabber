<?php declare(strict_types = 1);

namespace App\Services\Tesseract;

use Illuminate\Support\Facades\Log;

class OcrScanner implements Scanner
{

	/** @var string */
	private $trainDataPath;

	public function __construct(string $trainDataPath)
	{
		$this->trainDataPath = $trainDataPath;
	}

	public function scan(
		string $image,
		string $outputPath,
		string $outputExtension = 'txt'
	): string {
		$script = sprintf('export TESSDATA_PREFIX=%s; ', $this->trainDataPath);
		$script .= sprintf('tesseract -l ces %s %s >/dev/null 2>&1', $image, $outputPath);

		Log::debug('Calling ' . $script);
		exec($script);

		return $outputPath . '.' . $outputExtension;
	}
}
