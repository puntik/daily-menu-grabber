<?php declare(strict_types = 1);

namespace App\Services\Tesseract;

interface Scanner
{

	public function scan(string $image, string $outputPath, string $outputExtension = 'txt'): string;
}
