<?php declare(strict_types = 1);

namespace App\Services\FoodResolvers;

class FavoriteResolver
{

	/** @var array */
	private $patterns;

	public function __construct()
	{
		$this->patterns = [
			'guláš(?:em)?',
			'stro?ganoff?',
			'svíčkov(á|ou)',
			'rajsk(á|ou)',
			'koprov(á|ou|ka)',
			'ri[sz]ott?(o|em|u)',
		];
	}

	public function isFavorite(string $item): bool
	{
		foreach ($this->patterns as $pattern) {
			if (preg_match('/' . $pattern . '/iu', $item, $matches) === 1) {
				return true;
			}
		}

		return false;
	}
}
