<?php declare(strict_types = 1);

namespace App\Services\Zomato;

use GuzzleHttp\Client as HttpClient;
use Illuminate\Support\Facades\Log;

class Client
{

	public const BASE_URL = 'https://developers.zomato.com/api/v2.1';

	/** @var HttpClient */
	private $httpClient;

	/** @var string */
	private $userKey;

	public function __construct(
		HttpClient $httpClient,
		string $userKey
	) {
		$this->httpClient = $httpClient;
		$this->userKey    = $userKey;
	}

	public function dailyMenu(int $restaurantId): ?array
	{
		try {
			$response = $this->httpClient->get(self::BASE_URL . '/dailymenu', [
				'headers' => [
					'Accept'   => 'application/json',
					'user_key' => $this->userKey,
				],
				'query'   => [
					'res_id' => $restaurantId,
				],
			]);

			$json = $response->getBody()->getContents();

			return \GuzzleHttp\json_decode($json, true);
		} catch (\Throwable $e) {
			// invalid user_key
			// invalid restaurant id
			// invalid json to decode
			// daily limit over

			Log::error($e->getMessage());
		}

		return null;
	}
}
