<?php

namespace App\Providers;

use App\Services\Grabbers\GrabbersAggregator;
use App\Services\Zomato\Client as ZomatoClient;
use GuzzleHttp\Client as HttpClient;
use Illuminate\Support\ServiceProvider;

class DailyMenuProvider extends ServiceProvider
{

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        /*
         * Basic clients and services
         */
        $this->app->bind(ZomatoClient::class, function ($app): ZomatoClient {
            $httpClient = $app->make(HttpClient::class);

            return new ZomatoClient($httpClient, config('zomato.user_key'));
        });

        $this->app->tag(
            [
                \App\Services\Grabbers\NaRozhraniGrabber::class,
                \App\Services\Grabbers\FerdinandaGrabber::class,
            ],
            'grabbers'
        );

        /*
         * Grabbers aggregator
         */
        $this->app->bind(GrabbersAggregator::class, function ($app): GrabbersAggregator {
            return new GrabbersAggregator($app->tagged('grabbers'));
        });
    }

}
