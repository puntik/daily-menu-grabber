<?php

namespace App\Console\Commands;

use App\Jobs\ImportDailyMenuJob;
use App\Model\Restaurant;
use Illuminate\Console\Command;

class ImportDailyMenu extends Command
{

    /**
     * @var string
     */
    protected $signature = 'menu:import';

    /**
     * @var string
     */
    protected $description = 'Download and process daily menu and save them into cache';

    public function handle(): int
    {
        Restaurant::whereStatus(1)
                  ->get()
                  ->each(function (Restaurant $restaurant) {
                      $this->info(sprintf(
                          'Invoking import for #%d "%s".',
                          $restaurant->id,
                          $restaurant->name
                      ));

                      dispatch(new ImportDailyMenuJob($restaurant));
                  });

        return self::SUCCESS;
    }

}
