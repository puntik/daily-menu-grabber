<?php

namespace App\Model\Filters;

use Abetzi\EloquentFilter\QueryFilter;

class RestaurantFilter extends QueryFilter
{

    public function title(string $title)
    {
        $this->query->where('title', 'like', $title);
    }

}
