<?php

namespace App\Model;

use Abetzi\EloquentFilter\Filterable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Restaurant extends Model
{

    use Filterable;

    protected $fillable = [
        'grabber',
        'name',
        'status',
        'menu_url',
        'url',
    ];

    /*
     * Attributes
     */

    public function getCacheKeyAttribute(): string
    {
        return sprintf('pubs:%s', Str::slug($this->name));
    }
}
