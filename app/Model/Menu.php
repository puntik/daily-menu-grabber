<?php declare(strict_types = 1);

namespace App\Model;

class Menu
{

    /**
     * @var string
     */
    private string $pubName;

    /**
     * @var Dish[]|array
     */
    private array $dishes;

    public function __construct(string $pubName, array $dishes)
    {
        $this->pubName = $pubName;
        $this->dishes  = $dishes;
    }

    public function toArray(): array
    {
        $menu = array_map(function (Dish $dish) {
            return [
                'meat'  => $dish->getName(),
                'price' => $dish->getPrice(),
            ];
        }, $this->dishes);

        return [
            'menu' => $menu,
            'pub'  => $this->pubName,
        ];
    }
}
