<?php declare(strict_types = 1);

namespace App\Model;

class Dish
{

    private string $name;

    private ?string $price;

    public function __construct(string $name, ?string $price = null)
    {
        $this->name  = $name;
        $this->price = $price;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getPrice(): string
    {
        return $this->price ?? '';
    }
}
