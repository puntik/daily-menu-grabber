<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Carbon;

class OpenHoursChecker
{

	public function handle($request, Closure $next)
	{
		if (! $request->has('forced') && $this->isClosed()) {
			return redirect()->to('/closed');
		}

		return $next($request);
	}

	private function isOpenHours()
	{
		$now = Carbon::now();

		// is working day?
		if ($now->isSaturday() || $now->isSunday()) {
			return false;
		}

		// today between 10:15 a 15:00
		$start = Carbon::today()->setTime(10, 25);
		$end   = Carbon::today()->setTime(15, 00);

		return $now->greaterThan($start) && $now->lessThan($end);
	}

	private function isClosed()
	{
		return ! $this->isOpenHours();
	}
}
