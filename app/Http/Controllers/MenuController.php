<?php

namespace App\Http\Controllers;

use App\Http\Middleware\OpenHoursChecker;
use App\Model\Restaurant;

class MenuController extends Controller
{

    public function __construct()
    {
        // openhours protection
        $this->middleware(OpenHoursChecker::class);
    }

    public function __invoke()
    {
        $restaurants = Restaurant::whereStatus(true)
                                 ->orderByDesc('id')
                                 ->get();

        $restaurants->transform(function (Restaurant $restaurant) {
            $info             = cache()->get($restaurant->cache_key);
            $restaurant->menu = $info['menu'];

            return $restaurant;
        });

        return view('menus', compact('restaurants'));
    }

}
