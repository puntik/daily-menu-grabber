<?php

use Illuminate\Support\Facades\Route;

Route::get('/', 'MenuController');

Route::get('/closed', function () {
	return view('closed');
});
