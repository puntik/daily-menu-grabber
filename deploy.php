<?php

namespace Deployer;

require 'recipe/laravel.php';

// Project name
set('application', 'daily-menu');

// Project repository
set('repository', 'git@bitbucket.org:puntik/daily-menu-grabber.git');

// [Optional] Allocate tty for git clone. Default value is false.
set('git_tty', true);

// Shared files/dirs between deploys 
add('shared_files', []);
add('shared_dirs', []);

// Writable dirs by web server 
add('writable_dirs', []);

// Hosts

host('daily-menu.rem.cz')
	->set('branch', 'master')
	->hostname('o')
	->set('deploy_path', '~/www/rem.cz/{{application}}');

// Tasks

task('build', function () {
	run('cd {{release_path}} && build');
});

task('fpm:reload', function () {
	run('sudo /etc/init.d/php-fpm reload');
});

before('deploy:symlink', 'artisan:migrate');
after('deploy:failed', 'deploy:unlock');
after('deploy:symlink', 'fpm:reload');
