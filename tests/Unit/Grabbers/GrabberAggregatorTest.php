<?php

namespace Unit\Grabbers;

use App\Services\Grabbers\GrabbersAggregator;
use App\Services\Grabbers\NaRozhraniGrabber;
use App\Services\Grabbers\UVilemaGrabber;
use PHPUnit\Framework\TestCase;

class GrabberAggregatorTest extends TestCase
{

	// @todo: create aggregator - test constructor

	/** @var GrabbersAggregator */
	private $aggregator;

	protected function setUp(): void
	{
		parent::setUp();

		$httpClient = \Mockery::mock(\GuzzleHttp\Client::class);

		$grabbers = [
			new NaRozhraniGrabber($httpClient),
			new UVilemaGrabber($httpClient),
		];

		$this->aggregator = new GrabbersAggregator($grabbers);
	}

	/**
	 * @test
	 */
	public function itReturnsListOfRegisteredGrabbers()
	{
		// When
		$list = $this->aggregator->list();

		// Than
		$this->assertCount(2, $list);
	}

	/**
	 * @test
	 */
	public function itReturnsGrabberArray()
	{
		// When
		$foundGrabbers = $this->aggregator->grabbers();

		// Than
		$this->assertCount(2, $foundGrabbers);
	}
}
