<?php

namespace Unit\Grabbers;

use App\Services\Grabbers\FerdinandaGrabber;
use GuzzleHttp\Client;
use PHPUnit\Framework\TestCase;
use Tests\Unit\Grabbers\Traits\HasMockedGuzzleClient;

class FerdinandaGrabberTest extends TestCase
{

    use HasMockedGuzzleClient;

    private Client $client;

    protected function setUp(): void
    {
        $body         = file_get_contents(__DIR__ . '/ferdinanda.html');
        $this->client = $this->getHttpClient($body);

        parent::setUp();
    }

    /**
     * @test
     */
    public function it_downloads_data_from_ferdinanda_web()
    {
        // Given
        $grabber = new FerdinandaGrabber($this->client);

        // When
        $result = $grabber->grab();

        // Then
        $this->assertIsArray($result);
        $this->assertArrayHasKey('pub', $result);
        $this->assertArrayHasKey('menu', $result);
        $this->assertCount(15, $result['menu']);
    }
}
