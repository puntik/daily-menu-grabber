<?php declare(strict_types = 1);

namespace Unit\Grabbers;

use App\Services\Grabbers\PubDejviceGrabber;
use Illuminate\Support\Carbon;
use Tests\TestCase;
use Tests\Unit\Grabbers\Traits\HasMockedGuzzleClient;

class PubDejviceGrabberTest extends TestCase
{

	use HasMockedGuzzleClient;

	/**
	 * @test
	 */
	public function itParsesPubDejviceMenu()
	{
		Carbon::setTestNow(Carbon::create(2018, 10, 22)); // set fake date, monday 10/22

		$body   = file_get_contents(__DIR__ . '/data/pub-praha-6-dejvice.html');
		$client = $this->getHttpClient($body);

		$grabber = new PubDejviceGrabber($client);

		$data = $grabber->grab();

		// $this->assertCount(2, array_keys($data));
		$this->assertCount(7, $data['menu']);


	}
}
