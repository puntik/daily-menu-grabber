<?php

namespace Unit\Grabbers;

use GuzzleHttp\Client;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use Tests\TestCase;

class NaHradcanskeGrabberTest extends TestCase
{

	/**
	 * @test
	 */
	public function itCanGrabNaHradcanskeMenu()
	{
		// Given
		$body       = file_get_contents(__DIR__ . '/data/na-hradcanske.html');
		$httpClient = $this->getHttpClient($body);

		$grabber = new \App\Services\Grabbers\NaHradcanskeGrabber($httpClient);

		// When
		$menu = $grabber->grab();

		// Then
		$this->assertArrayHasKey('pub', $menu);
		$this->assertArrayHasKey('menu', $menu);

		$this->assertCount(13, $menu['menu']);
	}

	private function getHttpClient(string $body)
	{
		// Create a mock and queue two responses.
		$mock = new MockHandler([
			new Response(200, [], $body),
		]);

		$handler = HandlerStack::create($mock);

		return new Client(['handler' => $handler]);
	}
}
