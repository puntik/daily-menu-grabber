<?php

namespace Unit\Grabbers;

use GuzzleHttp\Client;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use Illuminate\Support\Carbon;
use Mockery;
use Tests\TestCase;

class ArgumentGrabberTest extends TestCase
{

	/**
	 * @test
	 */
	public function itCanGrabAndParseArgumentDailyMenu()
	{
		/*
		 * Given
		 */
		$body       = file_get_contents(__DIR__ . '/argument.html');
		$httpClient = $this->getHttpClient($body);
		Carbon::setTestNow(Carbon::create(2018, 10, 18)); // set fake date
		$grabber = new \App\Services\Grabbers\ArgumentGrabber($httpClient);

		$data = $grabber->grab();

		$this->assertEquals(6, count($data['menu']));
	}

	private function getHttpClient(string $body)
	{
		// Create a mock and queue two responses.
		$mock = new MockHandler([
			new Response(200, [], $body),
		]);

		$handler = HandlerStack::create($mock);

		return new Client(['handler' => $handler]);
	}

}
