<?php declare(strict_types = 1);

namespace Tests\Unit\Grabbers\Traits;

use GuzzleHttp\Client;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;

trait HasMockedGuzzleClient
{

	public function getHttpClient(string $body): Client
	{
		// Create a mock and queue two responses.
		$mock = new MockHandler([
			new Response(200, [], $body),
		]);

		$handler = HandlerStack::create($mock);

		return new Client(['handler' => $handler]);
	}
}
