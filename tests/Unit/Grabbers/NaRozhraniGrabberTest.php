<?php

namespace Unit\Grabbers;

use App\Services\Grabbers\NaRozhraniGrabber;
use GuzzleHttp\Client;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use Tests\TestCase;

class NaRozhraniGrabberTest extends TestCase
{

	/**
	 * @test
	 */
	public function itCanGrabAndParseNaRozhraniDailyMenu()
	{
		$body       = file_get_contents(__DIR__ . '/na-rozhrani.html');
		$httpClient = $this->getHttpClient($body);
		$grabber    = new NaRozhraniGrabber($httpClient);

		$response = $grabber->grab();

		$this->assertCount(7, $response['menu']);
	}

	private function getHttpClient(string $body)
	{
		// Create a mock and queue two responses.
		$mock = new MockHandler([
			new Response(200, [], $body),
		]);

		$handler = HandlerStack::create($mock);

		return new Client(['handler' => $handler]);
	}

}
