<?php declare(strict_types = 1);

namespace Unit\Grabbers;

use App\Services\Grabbers\CanzoneGrabber;
use GuzzleHttp\Client;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use Illuminate\Support\Carbon;
use Tests\TestCase;

class CanzoneGrabberTest extends TestCase
{

	/**
	 * @test
	 */
	public function itCanGrabAndParseCanzoneDailyMenu()
	{
		$body       = file_get_contents(__DIR__ . '/canzone.html');
		$httpClient = $this->getHttpClient($body);
		Carbon::setTestNow(Carbon::create(2018, 10, 18)); // set fake date
		$grabber = new CanzoneGrabber($httpClient);

		$response = $grabber->grab();

		$this->assertCount(5, $response['menu']);
	}

	private function getHttpClient(string $body)
	{
		// Create a mock and queue two responses.
		$mock = new MockHandler([
			new Response(200, [], $body),
		]);

		$handler = HandlerStack::create($mock);

		return new Client(['handler' => $handler]);
	}
}
