<?php

namespace Tests\Unit\Grabbers;

use App\Services\Grabbers\SokolovnaDejviceGrabber;
use Tests\TestCase;
use Tests\Unit\Grabbers\Traits\HasMockedGuzzleClient;

class SokolovnaDejviceGrabberTest extends TestCase
{

	use HasMockedGuzzleClient;

	/** @var string */
	private $sourceFile;

	protected function setUp(): void
	{
		parent::setUp();

		$this->sourceFile = __DIR__ . '/sokolovna-dejvice.htm';
	}

	/**
	 * @test
	 */
	public function itGrabsSokolovnaDejviceData()
	{
		$body   = file_get_contents($this->sourceFile);
		$client = $this->getHttpClient($body);

		$grabber = new SokolovnaDejviceGrabber($client);
		$result  = $grabber->grab();

		$this->assertCount(31, $result['menu']);
	}
}
