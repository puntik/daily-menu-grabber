<?php declare(strict_types = 1);

namespace Unit\Grabbers;

use App\Services\Grabbers\BruskaGrabber;
use GuzzleHttp\Client;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use Tests\TestCase;

class BruskaGrabberTest extends TestCase
{

	/**
	 * @test
	 */
	public function itCanDownloadBruskaMenuFromZomatoAndParse()
	{
		$json         = $this->mockData();
		$httpClient   = $this->getHttpClient($json);
		$zomatoClient = new \App\Services\Zomato\Client($httpClient, 'secret');

		$grabber = new BruskaGrabber($zomatoClient);

		$menu = $grabber->grab();

		$this->assertCount(15, $menu['menu']);
	}

	private function mockData(): string
	{
		$data = <<<JSON
{
  "daily_menus": [
    {
      "daily_menu": {
        "daily_menu_id": "19997118",
        "start_date": "2018-10-09 00:00:00",
        "end_date": "2018-10-09 23:59:59",
        "name": "",
        "dishes": [
          {
            "dish": {
              "dish_id": "683861112",
              "name": "Dnes má svátek Štefan, Sára",
              "price": ""
            }
          },
          {
            "dish": {
              "dish_id": "683861113",
              "name": "### Šéfkuchař dnes připravuje na lávovém grilu:",
              "price": ""
            }
          },
          {
            "dish": {
              "dish_id": "683861114",
              "name": "200g Vepřový steak gratinovaný s broskví a sýrem, americké brambory       ",
              "price": "169 Kč"
            }
          },
          {
            "dish": {
              "dish_id": "683861115",
              "name": "### Menu",
              "price": "1 Kč"
            }
          },
          {
            "dish": {
              "dish_id": "683861116",
              "name": "Polévka",
              "price": ""
            }
          },
          {
            "dish": {
              "dish_id": "683861117",
              "name": "150g Krůtí prso špikované anglickou slaninou s kus - kusem a červenou řepou       ",
              "price": "139 Kč"
            }
          },
          {
            "dish": {
              "dish_id": "683861118",
              "name": "Menu",
              "price": "2 Kč"
            }
          },
          {
            "dish": {
              "dish_id": "683861119",
              "name": "Polévka",
              "price": ""
            }
          },
          {
            "dish": {
              "dish_id": "683861120",
              "name": "150g Vepřová plec po cikánsku s dušenou rýží, nebo houskovým knedlíkem       ",
              "price": "139 Kč"
            }
          },
          {
            "dish": {
              "dish_id": "683861121",
              "name": "### Polévky",
              "price": ""
            }
          },
          {
            "dish": {
              "dish_id": "683861122",
              "name": "Jihočeská kulajda s houbami       ",
              "price": "39 Kč"
            }
          },
          {
            "dish": {
              "dish_id": "683861123",
              "name": "Česneková s krutony       ",
              "price": "39 Kč"
            }
          },
          {
            "dish": {
              "dish_id": "683861124",
              "name": "### Hotová jídla",
              "price": ""
            }
          },
          {
            "dish": {
              "dish_id": "683861125",
              "name": "150g Smažený květák s pažitkovým bramborem a domácí tatarkou       ",
              "price": "115 Kč"
            }
          },
          {
            "dish": {
              "dish_id": "683861126",
              "name": "150g Krůtí prso špikované anglickou slaninou s kus - kusem a červenou řepou       ",
              "price": "127 Kč"
            }
          },
          {
            "dish": {
              "dish_id": "683861127",
              "name": "150g Vepřová plec po cikánsku s dušenou rýží, nebo houskovým knedlíkem       ",
              "price": "123 Kč"
            }
          },
          {
            "dish": {
              "dish_id": "683861128",
              "name": "150g Thajská kuřecí směs s dušenou rýží       ",
              "price": "125 Kč"
            }
          },
          {
            "dish": {
              "dish_id": "683861129",
              "name": "2 ks Buřty pečené na černém pivu s čerstvým pečivem       ",
              "price": "99 Kč"
            }
          },
          {
            "dish": {
              "dish_id": "683861130",
              "name": "150g Gratinovaný lilek s masovou směsí a sýrem, šťouchané bramborey       ",
              "price": "123 Kč"
            }
          },
          {
            "dish": {
              "dish_id": "683861131",
              "name": "Domácí tagliatelle ze semolinové mouky  s vepřovou panenkou, gorgonzolou a smetanou       ",
              "price": "119 Kč"
            }
          },
          {
            "dish": {
              "dish_id": "683861132",
              "name": "### Saláty",
              "price": ""
            }
          },
          {
            "dish": {
              "dish_id": "683861133",
              "name": "150g Salát z červené řepy       ",
              "price": "45 Kč"
            }
          },
          {
            "dish": {
              "dish_id": "683861134",
              "name": "250g Míchaný zeleninový salát s grilovaným hermelínem a brusinkami       ",
              "price": "129 Kč"
            }
          },
          {
            "dish": {
              "dish_id": "683861135",
              "name": "### Dezert na konec",
              "price": ""
            }
          },
          {
            "dish": {
              "dish_id": "683861136",
              "name": "Palačinka s marmeládou a šlehačkou       ",
              "price": "35 Kč"
            }
          }
        ]
      }
    }
  ],
  "status": "success"
}

JSON;

		return $data;
	}

	private function getHttpClient(string $body)
	{
		// Create a mock and queue two responses.
		$mock = new MockHandler([
			new Response(200, [
				'Content-Type' => 'application/json',
			], $body),
		]);

		$handler = HandlerStack::create($mock);

		return new Client(['handler' => $handler]);
	}
}
