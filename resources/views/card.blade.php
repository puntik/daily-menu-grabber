@extends('layout')

@section('main')

	<div class="card-columns">
		<div class="card">
			<img class="card-img-top" src="{{ asset('covers/m2.jpg') }}" alt="Card image cap">
			<div class="card-header">
				Card title
			</div>
			<ul class="list-group list-group-flush">
				<li class="list-group-item">Cras justo odio</li>
				<li class="list-group-item">Dapibus ac facilisis in</li>
				<li class="list-group-item">Morbi leo risus</li>
				<li class="list-group-item">Porta ac consectetur ac</li>
				<li class="list-group-item">Vestibulum at eros</li>
				<li class="list-group-item">Vestibulum at eros</li>
				<li class="list-group-item">Vestibulum at eros</li>
				<li class="list-group-item">Vestibulum at eros</li>
				<li class="list-group-item">Vestibulum at eros</li>
			</ul>
		</div>

		<div class="card">
			<img class="card-img-top" src="{{ asset('covers/m2.jpg') }}" alt="Card image cap">
			<div class="card-header">
				Card title
			</div>
			<ul class="list-group list-group-flush">
				<li class="list-group-item">Cras justo odio</li>
				<li class="list-group-item">Cras justo odio</li>
				<li class="list-group-item">Dapibus ac facilisis in</li>
				<li class="list-group-item">Morbi leo risus</li>
				<li class="list-group-item">Porta ac consectetur ac</li>
				<li class="list-group-item">Vestibulum at eros</li>
			</ul>
		</div>

		<div class="card">
			<img class="card-img-top" src="{{ asset('covers/m2.jpg') }}" alt="Card image cap">
			<div class="card-header">
				Card title
			</div>
			<ul class="list-group list-group-flush">
				<li class="list-group-item">Cras justo odio</li>
				<li class="list-group-item">Dapibus ac facilisis in</li>
				<li class="list-group-item">Morbi leo risus</li>
				<li class="list-group-item">Porta ac consectetur ac</li>
				<li class="list-group-item">Porta ac consectetur ac</li>
				<li class="list-group-item">Porta ac consectetur ac</li>
				<li class="list-group-item">Vestibulum at eros</li>
			</ul>
		</div>

		<div class="card">
			<img class="card-img-top" src="{{ asset('covers/m2.jpg') }}" alt="Card image cap">
			<div class="card-header">
				Card title
			</div>
			<ul class="list-group list-group-flush">
				<li class="list-group-item">Cras justo odio</li>
				<li class="list-group-item">Dapibus ac facilisis in</li>
				<li class="list-group-item">Morbi leo risus</li>
				<li class="list-group-item">Porta ac consectetur ac</li>
				<li class="list-group-item">Vestibulum at eros</li>
			</ul>
		</div>
	</div>

@endsection
