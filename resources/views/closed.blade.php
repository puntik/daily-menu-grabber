@extends('layout')

@section('main')
	<div class="container mx-auto">
		<div class="my-2 bg-red-200 border border-red-900 p-2 text-red-900 rounded" role="alert">
			<h4 class="font-bold">Pracuj!</h4>
			<p>Je mi líto, ale momentálně je zavřeno. Otvíráme v půl jedenácté.</p>
			<p>Jídelní lístek je v provozu pouze pracovní dny od 10:30 do 15:00.</p>
		</div>
	</div>
@endsection
