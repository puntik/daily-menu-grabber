@inject('favoriteResolver', 'App\Services\FoodResolvers\FavoriteResolver')
<div class="shadow">
	<img class="" src="{{ asset('covers/m' . \Illuminate\Support\Arr::random([1,2,3]). '.jpg') }}" alt="Card image cap">
	<div class="bg-gray-500 p-2 text-white">
		<h2>{{ $restaurant->name }}
			@if($restaurant->menu_url !== null)
				<small>
					<a href="{{ $restaurant->menu_url }}">
						<i class="fas fa-external-link-alt"></i>
					</a>
				</small>
			@endif
		</h2>
	</div>

	@if($restaurant->menu)
		<ul class="bg-white">
			@foreach($restaurant->menu as $row)
				<li class="text-sm border-b px-2 py-3">
					@if($favoriteResolver->isFavorite($row['meat']))
						<i class="fa fa-star text-yellow-500"></i>
					@endif
					{{ $row['meat'] }}
					{{ $row['price'] }}
				</li>
			@endforeach
		</ul>
	@else
		<div class="p-2 bg-white">
			<div class="bg-red-200 border border-red-800 rounded text-red-800 p-2" role="alert">
				Data ještě nejsou zpracována nebo došlo k nějaké chybě.
			</div>
		</div>
	@endif
</div>
