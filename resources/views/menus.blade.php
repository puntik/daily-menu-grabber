@extends('layout')

@section('main')
	<div class="" style="">
		@foreach($restaurants as $restaurant)
			<div class="px-2 py-2">
				@component('pub', ['restaurant' => $restaurant])
				@endcomponent
			</div>
		@endforeach
	</div>
@endsection
