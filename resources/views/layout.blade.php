<html>
	<head>
		<title>Denní menu</title>
		<link rel="stylesheet" href="{{ asset('css/app.css') }}">
		<script async src="https://www.googletagmanager.com/gtag/js?id=UA-123055630-2"></script>
		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css" integrity="sha384-5sAR7xN1Nv6T6+dT2mhtzEpVJvfS3NScPQTrOxhwjIuvcA67KV2R5Jz6kr4abQsz" crossorigin="anonymous">

		<script>
			window.dataLayer = window.dataLayer || [];

			function gtag() {
				dataLayer.push(arguments);
			}

			gtag('js', new Date());

			gtag('config', 'UA-123055630-2');
		</script>
	</head>

	<body class="bg-gray-200">
		<nav class="p-2 px-4 bg-gray-600 shadow mb-2">
			<a class="text-white" href="{{ url('/') }}"><i class="fa fa-utensils" style="margin-right: 5px;"></i>
				Denní menu (okolí Hradčanské)
			</a>
		</nav>

		<div class="container mx-auto">
			@yield('main')
		</div>
	</body>
</html>

