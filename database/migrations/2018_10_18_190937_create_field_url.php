<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFieldUrl extends Migration
{

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('restaurants', function (Blueprint $table) {
			$table->string('menu_url', 256)->nullable();
			$table->string('url', 256)->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('restaurants', function (Blueprint $table) {
			$table->dropColumn([
				'menu_url',
				'url',
			]);
		});
	}
}
